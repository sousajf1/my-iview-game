#include <functional>
#include <random>
#include <utility>
#include <iostream>
#include <iterator>
#include "cilinder.h"

std::vector<char> Cilinder::getCharset(){
    return std::vector<char>({
                    'A','B','C','D','E','F',
                });
}

std::vector<char> Cilinder::randomVec(std::function<char()> randChar, size_t length){
    std::vector<char> v(length);
    std::generate(v.begin(), v.end(), std::move(randChar)); //move lambda?
    return v;
}

template<typename T>
std::vector<char> randomVecTemplate(T&& randChar, size_t length){
    std::vector<char> v(length);
    std::generate(v.begin(), v.end(), randChar);
    return v;
}

std::vector<char> Cilinder::init(){
    auto chSet = getCharset();
    std::default_random_engine rng(std::random_device{}());
    std::uniform_int_distribution<> dist(0, chSet.size() - 1);
    const auto randChar = [chSet, &dist, &rng](){
        return chSet[dist(rng)];
    };
    constexpr size_t length = 6;
    const auto v = randomVec(randChar, length);
    chSet.insert(chSet.end(), v.begin(), v.end());
    std::shuffle(chSet.begin(), chSet.end(), rng);
    return chSet;
}

char Cilinder::spin() {
    std::default_random_engine rng(std::random_device{}());
    std::uniform_int_distribution<> dist(0, std::distance(values.begin(), values.end()) - 1);
    auto vi = values.begin();
    std::advance(vi , dist(rng)); // lvalue n pode aqui. auto vi = begint()=
    return *vi;
}

#ifndef UNTITLED_CILINDER_H
#define UNTITLED_CILINDER_H

#include <vector>
#include <functional>

class Cilinder{
public:
    std::vector<char> values;
public:
    Cilinder() : values(init()){}
    Cilinder(Cilinder&&) = delete;
    Cilinder (const Cilinder&) = delete;
    Cilinder& operator=(const Cilinder&) = delete;
    Cilinder& operator=(Cilinder&&) = delete;
    ~Cilinder() = default;

    static std::vector<char> init();
    char spin();
    static std::vector<char> getCharset();
    static std::vector<char> randomVec(std::function<char()> randChar, size_t length);
};
#endif //UNTITLED_CILINDER_H

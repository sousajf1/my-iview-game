#include <iostream>
#include "game.h"

const std::map<const std::string, const int> Game::prizeMap = {
        {"AAA", 2},
        {"BBB", 3},
        {"CCC", 4},
        {"DDD", 5},
        {"EEE", 6},
        {"FFF", 7},
};

void Game::run(){
    givePrize(spin());
}

void Game::showCredits() {
    player.showCredits();
}

const std::string Game::spin() {
    std::string spinResult;
    for(auto& c : cilinders){
        spinResult += c.spin();
        std::cout << "This is the result of the spin: " << spinResult << '\n';
    }
    return spinResult;
}

void Game::givePrize(const std::string& spinResult) {
    auto it = prizeMap.find(spinResult);
    if (it != prizeMap.end()) {
        auto prize = calculatePrize(it->second);
        updatePlayerWallet(prize);
        std::cout << "you won: " << prize << '\n';
    }else{
        std::cout << "No prize ... try again" << '\n';
    }
}

int Game::calculatePrize(int multiplier) const {
    return bet * multiplier;
}


void Game::updatePlayerWallet(int value) {
    player.setCredits(value);
}

void Game::placeBet(int betValue){
    bet = betValue;
    updatePlayerWallet(-bet);
}

Player Game::getPlayer() {
    return player;
}


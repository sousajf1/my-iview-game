#ifndef UNTITLED_GAME_H
#define UNTITLED_GAME_H

#include "player.h"
#include "cilinder.h"
#include <map>

class Game{
    Player player;
    std::array<Cilinder, 3> cilinders;
    static const std::map<const std::string, const int> prizeMap;
    int bet;
    int prize;
public:
    int calculatePrize(int multiplier) const;
    void updatePlayerWallet(int value);
    void run();
    const std::string spin();
    void placeBet(int betValue);
    void showCredits();
    void givePrize(const std::string& spinResult);
    Player getPlayer();
};
#endif //UNTITLED_GAME_H

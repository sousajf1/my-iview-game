#include <iostream>
#include "game.h"

int main() {
    std::cout << "*** FMQ SLOT MACHINE ***" << '\n';
    Game game;
    game.showCredits();
    std::cout << "Carteira: " << game.getPlayer().getWallet().getCredits() << '\n';
    std::string input;
    do{
        std::cout << "Please place your bet: " << '\n';
        int bet;
        std::cin >> bet;
        while(std::cin.fail()) {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
            std::cout << "Enter a NUMBER to place your bet please: ";
            std::cin >> bet;
        }
        game.placeBet(bet);
        game.run();
        game.showCredits();
        std::cout << "Want to play again? " << '\n';
        std::cin >> input;
    }while(input != "n");
    return 0;
}

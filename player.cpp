#include <iostream>
#include "player.h"

void Player::placeBet(const int value){
    wallet.setCredits(-value);
}

void Player::setCredits(const int prize){
    wallet.setCredits(prize);
}

void Player::showCredits() {
    std::cout << "Carteira: " << wallet.getCredits() << '\n';
}

Wallet Player::getWallet() {
    return wallet;
}

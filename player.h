#ifndef UNTITLED_PLAYER_H
#define UNTITLED_PLAYER_H

#include <memory>
#include "wallet.h"

class Wallet;

class Player{
    Wallet wallet;
public:
    void placeBet(const int value);
    void setCredits(const int prize);
    void showCredits();
    Wallet getWallet();
};

#endif //UNTITLED_PLAYER_H

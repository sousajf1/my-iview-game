#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "../catch2/catch.hpp"
#include "../wallet.h"

constexpr int WALLET_INITIAL_VALUE = 10000;

TEST_CASE( "CreditsBelowZeroShouldFail", "[setCredits]" ) {
    Wallet w;
    w.setCredits(-100001);
    REQUIRE( w.getCredits() == WALLET_INITIAL_VALUE );
}

TEST_CASE( "PrizeShouldBeAddedToTheCredits", "[setCredits]" ) {
    Wallet w;
    w.setCredits(100);
    REQUIRE( w.getCredits() == 10100 );
}

TEST_CASE( "BetShouldBeDiscountedToTheCredits", "[setCredits]" ) {
    Wallet w;
    w.setCredits(-100);
    REQUIRE( w.getCredits() == 9900 );
}
/*
TEST_CASE( "CreditsAboveIntMaxShouldFail", "[setCredits]" ) {
    Wallet w;
    w.setCredits(std::numeric_limits<int>::max());
    REQUIRE( w.getCredits() == WALLET_INITIAL_VALUE );
}*/
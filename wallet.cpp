#include <iostream>
#include "wallet.h"

int Wallet::getCredits() const{
    return credits;
}

void Wallet::setCredits(const int c){
    if (credits + c < 0){
        std::cout << "You went broke bro!" << '\n';
    }else{
        credits += c;
    }
}



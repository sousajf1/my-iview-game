#ifndef UNTITLED_WALLET_H
#define UNTITLED_WALLET_H
class Wallet{
    int credits = 10000;
public:
    int getCredits() const;
    void setCredits(const int c);
};
#endif //UNTITLED_WALLET_H
